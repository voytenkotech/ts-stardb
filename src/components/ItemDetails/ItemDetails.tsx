import React, { useEffect, useState } from 'react';
import SwapiService from '../../swapiService';
import Spinner from '../Spinner';

import './ItemDetails.css';

const ItemDetails = ({itemId, getData, getImageUrl} : any) => {

    const [item, setItem] = useState()

    useEffect(() => {
        updateItem(itemId)
    }, [itemId])
    
    const [imageUrl, setImageUrl] = useState()

    const updateItem = (itemId: number) => {
        const id = itemId;
        if (!id) {
            return;
        }
        getData(id)
            .then((item: any | undefined) => {
                setItem(item)
                setImageUrl(getImageUrl(id))
            })
    }

    if (!item) {
        return <span>Select a person from a list</span>
    }

    const { id, name, } = item;
    if (id != itemId) {
        return <Spinner/>
    } else {
        return (
            <div className="person-details card">
                <div className="d-flex">
                    <img alt="character"
                     className="person-image"
                     src={imageUrl}></img>
                </div>
                <div className="card-body">
                    <ul className='list-group list-group-flush'>
                        <li className="list-group-item">
                            <h5>{' '} {name}</h5>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default ItemDetails;