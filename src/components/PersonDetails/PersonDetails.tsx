import React, { useEffect, useState } from 'react';
import SwapiService from '../../swapiService';
import Spinner from '../Spinner';

import './PersonDetails.css';

const PersonDetails = ({personId} : any) => {

    const swapiService = new SwapiService()
    const [person, setPerson] = useState()

    useEffect(() => {
        updatePerson(personId)
    }, [personId])
    
    const updatePerson = (personId: number) => {
        const id = personId;
        if (!id) {
            return;
        }
        swapiService
            .getPerson(id)
            .then((person: any | undefined) => {
                setPerson(person)
            })
    }

    if (!person) {
        return <span>Select a person from a list</span>
    }

    const { id, name, gender, birthYear, eyeColor } = person;

    if (id != personId) {
        return <Spinner/>
    } else {
        return (
            <div className="person-details card">
                <div className="d-flex">
                    <img alt="character"
                     className="person-image"
                     src={`https://starwars-visualguide.com/assets/img/characters/${id}.jpg`}></img>
                </div>
                <div className="card-body">
                    <ul className='list-group list-group-flush'>
                        <li className="list-group-item">
                            <h5>{' '} {name}</h5>
                        </li>
                        <li className="list-group-item">
                            <span className="term">Gender: </span>
                            <span>{' '} {gender}</span>
                        </li>
                        <li className="list-group-item">
                            <span className="term">Birth Year: </span>
                            <span>{' '} {birthYear}</span>
                        </li>
                        <li className="list-group-item">
                            <span className="term">Eye Color: </span>
                            <span>{' '} {eyeColor}</span>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default PersonDetails;