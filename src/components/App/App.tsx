import React, { useState } from 'react';
import Header from '../Header';
import RandomPlanet from '../RandomPlanet';
import PeoplePage from '../PeoplePage';
import PlanetPage from '../PlanetPage';
import './App.css';
import PersonDetails from '../PersonDetails';
import ItemDetails from '../ItemDetails';
import SwapiService from '../../swapiService';


const App = () => {

    const swapiService = new SwapiService();
    return (

        <div className="app container">
            <Header />
            <RandomPlanet/>
            <PeoplePage/>
        </div>
    )
}

export default App;