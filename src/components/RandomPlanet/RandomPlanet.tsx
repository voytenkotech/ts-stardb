import React, { useEffect, useState } from 'react';
import SwapiService from '../../swapiService';
import './RandomPlanet.css';
import Spinner from '../Spinner';
import ErrorIndicator from '../ErrorIndicator';

const RandomPlanet = () => {

    const swapiService = new SwapiService()

    const [planet, setPlanet] = useState()
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState('')

    useEffect(() => {
        interval()

    }, [])
    const onError = (err: string) => {
        const newError = err;
        const newLoading = false
        setError(newError)
        setLoading(newLoading)
    };
    const onPlanetLoaded = (planet: any) => {
        const newLoading = false
        setPlanet(planet);
        setLoading(newLoading)

    }   
    const updatePlanet = () => {
        const id = Math.floor(Math.random() * 10 + 2)
        swapiService
            .getPlanet(id)
            .then(onPlanetLoaded)
            .catch(onError)
    }
    const interval = () => {
        setInterval(updatePlanet, 5000)
    }
    const hasData = !(loading || error);
        
    const errorMessage = error ? <ErrorIndicator /> : null;
    const spinner = loading && !error ? <Spinner /> : null;
    const content = hasData ? <PlanetView planet={planet}/> : null;

    return (
        <div className='random-planet d-flex'>
            {errorMessage}
            {spinner}
            {content}
        </div>
    )
}

export default RandomPlanet;

const PlanetView = ({ planet } : any) => {

    const {  id, 
        name,
        population,
        rotationPeriod, 
        diameter } = planet;

    return (
        <React.Fragment>
            <img 
            alt="Planet"
                src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`}>
            </img>
            <div 
            className="about-random-planet">
                    <h2>{name}</h2>
                    <ul className='list-group list-group-flush'>
                        <li>
                            <span 
                                className="term">
                                Population: 
                            </span>
                            <span>
                                {` ` + population}
                            </span>
                        </li>
                        <li>
                            <span 
                                className="term">
                                Rotation Period: 
                            </span>
                            <span>
                                {` ` + rotationPeriod}
                            </span>
                        </li>
                        <li>
                            <span 
                                className="term">
                                Diameter: 
                            </span>
                            <span>
                                {` ` + diameter}
                            </span>
                        </li>
                    </ul>
            </div>
        </React.Fragment>
    )
}