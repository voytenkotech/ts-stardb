import React from 'react';

import './Header.css';

const Header = () => {
    return (
        <div className="header d-flex">
            <h3>
                <a href="/home">
                    Star DB
                </a>
            </h3>

        </div>
    )
}

export default Header;