import React, {useEffect, useState, FC} from 'react';
import Spinner from '../Spinner';
import './ItemList.css'

const ItemList:FC<{onItemSelected: any, getData: any, children: any}> 
    = ({onItemSelected, getData, children}) => {

    const [state, setState] = useState();

    useEffect(() => {
        getData
        .then((peopleList: any) => {
            setState(peopleList)
        })

    }, [])
    const renderItems = (arr: []) => {
        return arr.map((item) => {

            const {id} = item
            const label = children(item)

            return (
                <li className="list-group-item"
                    key={id}
                    onClick={() => onItemSelected(id)}>
                        {label}
                </li>
            )   
        }    
    )}

    if (!state) {
        return <Spinner />
    }

    const items = renderItems(state)

    return (
        <ul className="item-list list-group mb-3">
            {items}
        </ul>
    )
}

export default ItemList;