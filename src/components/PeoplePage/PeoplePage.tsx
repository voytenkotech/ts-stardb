import React, {useState} from 'react';
import './PeoplePage.css';
import ItemList from '../ItemList';
import PersonDetails from '../PersonDetails';
import SwapiService from '../../swapiService';

const PeoplePage = () => {

    const Row = (left: any, right: any) => {
        return (
            <div className="row mb-2">
                <div className='col-md-6'>
                   {itemList}
                </div>
                <div className="col-md-6">
                    {personDetails}
                </div>
            </div>
        )
    }
    const swapiService = new SwapiService()
    const [person, setPerson] = useState()

    const onPersonSelected = (id: number | any) : number | any=> {
        setPerson(id)
    }

    const itemList = (
        <ItemList 
        onItemSelected={onPersonSelected}
        getData={swapiService.getAllPeople()}>
            {(i: any) => `${i.name}`}
            </ItemList>
    )

    const personDetails = (
        <PersonDetails personId={person}/>
    )
    return (
        <Row left={itemList} right={PersonDetails}/>
    )

}

export default PeoplePage