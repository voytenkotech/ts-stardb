import React from 'react';

import './Spinner.css';

const Spinner = () => {
    return (
        <div className="img-spin">
            <div className="loadingio-spinner-double-ring-yjqniluw5xl">
        <div className="ldio-mj9zj5psgn">
        <div></div>
        <div></div>
        <div><div></div></div>
        <div><div></div></div>
        </div></div>
        </div>

    )
}

export default Spinner;
