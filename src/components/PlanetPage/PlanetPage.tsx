import React, {useState} from 'react';
import './PlanetPage.css';
import ItemList from '../ItemList';
import PersonDetails from '../PersonDetails';
import SwapiService from '../../swapiService';

const PlanetPage = () => {

    const Row = (left: any, right: any) => {
        return (
            <div className="row mb-2">
                <div className='col-md-6'>
                    {itemList}
                </div>
                <div className="col-md-6">
                    {personDetails}
                </div>
             </div>
        )
    }
    const swapiService = new (SwapiService as any);
    const [person, setPerson] = useState()

    const onPersonSelected = (id:  any) : any=> {
        setPerson(id)
    }

    const itemList = (
        <ItemList 
            onItemSelected={onPersonSelected}
            getData={swapiService.getAllPlanets()}
            >
                {(i: any) => `${i.name}`}
            </ItemList>
    ) 
    const personDetails = (
        <PersonDetails fp={person}/>
    )
    return (
        <Row left={itemList} right={personDetails}/>
    )

}

export default PlanetPage